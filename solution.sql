--Find all artist that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%" OR name LIKE "%d" OR name LIKE "d%";

--find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

--Join the 'albums' and 'songs' tables. (Only show the album name,song name and song length)
SELECT albums.album_title AS "Album Name",
            songs.song_name AS "Song Title",
            songs.length AS Duration FROM albums 
            JOIN songs ON albums.id = songs.album_id;

--Join the 'artists' and 'albums' tables. (find all albums that has letter a in its name)
SELECT * FROM artists 
    JOIN albums ON artists.id = albums.artist_id
    WHERE album_title LIKE "%a%"
    OR album_title LIKE "a%"
    OR album_title LIKE "%a";


--Sort the albums in Z-A order (Show only the first 4 records)

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z)

SELECT * FROM albums 
    JOIN songs ON albums.id = songs.album_id
    ORDER BY album_title DESC;
    
SELECT * FROM albums 
    JOIN songs ON albums.id = songs.album_id
    ORDER BY song_name ASC;






